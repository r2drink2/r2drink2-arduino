// Arduino Nano Atmega 328
#include <Servo.h>
#include <Wire.h>


const int BASE_CONTROLLER_ADDRESS = 4;
const int VALVE_1_CONTROLLER_ADDRESS = 5;
const int VALVE_2_CONTROLLER_ADDRESS = 6;

const int VALVE_PINS[15] = { 17, 16, 15, 14, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

unsigned long VALVE_OPEN_EXPIRES[15] = { 0, 0, 0, 0, 0,
                                         0, 0, 0, 0, 0,
                                         0, 0, 0, 0, 0 };

byte VALVE_OPEN_STATUS[15] = { LOW, LOW, LOW, LOW, LOW,
                              LOW, LOW, LOW, LOW, LOW,
                              LOW, LOW, LOW, LOW, LOW };

const int LED_PIN = 13;


void beginPourValve(int valveNum, unsigned int pourMs) {
  Serial.println("ValveController Begin Valve Pour");
  Serial.println(valveNum, DEC);
  Serial.println(pourMs, DEC);
  VALVE_OPEN_EXPIRES[valveNum-1] = millis() + pourMs;
  digitalWrite(VALVE_PINS[valveNum-1], HIGH);
  VALVE_OPEN_STATUS[valveNum-1] = HIGH;
}


void setup() {
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  for(int i=0; i<15; i++) {
    pinMode(VALVE_PINS[i], OUTPUT);
    digitalWrite(VALVE_PINS[i], LOW);
  }

  Serial.begin(9600);
  Serial.println("ValveController Serial Initialized");
  Serial.println("ValveController GPIO Initialized");

  digitalWrite(LED_PIN, HIGH);
  delay(1000);
  digitalWrite(LED_PIN, LOW);
  delay(500);
  Serial.println("ValveController Startup Complete");
  
  //Wire.begin(VALVE_1_CONTROLLER_ADDRESS);
  Wire.begin(VALVE_2_CONTROLLER_ADDRESS);
  Wire.onReceive(onReceive);
  Wire.onRequest(onRequest);
  Serial.println("ValveController I2C Initialized");
}


void loop() {
  /*
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  for(int i=0; i<15; i++) {
    digitalWrite(VALVE_PINS[i], HIGH);
    delay(500);
  }
  digitalWrite(LED_PIN, LOW);
  delay(500);
  for(int i=0; i<15; i++) {
    digitalWrite(VALVE_PINS[i], LOW);
    delay(500);
  }
  return;
  //*/
  
  unsigned long cur_time = millis();
  for(int i=0; i<15; i++) {
    if(VALVE_OPEN_STATUS[i] == HIGH && VALVE_OPEN_EXPIRES[i] < cur_time) {
      digitalWrite(VALVE_PINS[i], LOW);
      VALVE_OPEN_STATUS[i] = LOW;
      Serial.print("Closing valve");
      Serial.println(i+1, DEC);
    }
  }
}

void onReceive(int count) {
  if(count == 3) {
    int targetValve = Wire.read();
    byte pourMs_1 = Wire.read();
    byte pourMs_2 = Wire.read();
    unsigned int targetPourMs = (pourMs_1 << 8) + pourMs_2;

    if(targetValve == 0) {
      for(int i=0; i<15; i++) {
        digitalWrite(VALVE_PINS[i], LOW);
        VALVE_OPEN_EXPIRES[i] = 0;
        VALVE_OPEN_STATUS[i] = LOW;
      }
      Serial.println("ValveController All Valves Closed");
    }
    else {
      beginPourValve(targetValve, targetPourMs);
    }
    
  }
}

void onRequest() {
  Wire.write(VALVE_OPEN_STATUS, 15);
}

