// Arduino Nano Atmega 328
#include <Servo.h>
#include <Wire.h>
#include "HX711.h"

const int VALVE_1_CONTROLLER_ADDRESS = 5;
const int VALVE_2_CONTROLLER_ADDRESS = 6;

const int SCALE_SDA_PIN = 12;
const int SCALE_SCLK_PIN = 11;
HX711 SCALE(SCALE_SDA_PIN, SCALE_SCLK_PIN);
long SCALE_OFFSET = 0;
double SCALE_CALIBRATION_GRAMS = 154.0; // Nexus 5 + Case
double SCALE_CALIBRATION_UNITS = 481924.0;
boolean ENABLE_SCALE = false;

double getWeight() {
  long reading = SCALE.read_average(5) - SCALE_OFFSET;
  reading = max(reading, 0);
  return ((double)reading)/SCALE_CALIBRATION_UNITS * SCALE_CALIBRATION_GRAMS;
}

const int DOOR_LEFT_SERVO_PIN = 6;
const int DOOR_RIGHT_SERVO_PIN = 7;
Servo DOOR_LEFT_SERVO;
Servo DOOR_RIGHT_SERVO;
void moveDoor(boolean opened) {
  if(opened) {
    DOOR_LEFT_SERVO.writeMicroseconds(600);
    DOOR_RIGHT_SERVO.writeMicroseconds(1500);
  }
  else {
    DOOR_LEFT_SERVO.writeMicroseconds(1500);
    DOOR_RIGHT_SERVO.writeMicroseconds(600);
  }
  DOOR_LEFT_SERVO.attach(DOOR_LEFT_SERVO_PIN);
  DOOR_RIGHT_SERVO.attach(DOOR_RIGHT_SERVO_PIN);
  delay(500);
  DOOR_LEFT_SERVO.detach();
  DOOR_RIGHT_SERVO.detach();
}


const int ARM_SERVO_PIN = 10;
Servo ARM_SERVO;
const int ARM_SERVO_CENTER_POS = 1350;
const int ARM_SERVO_RIGHT_POS = 1575;
const int ARM_SERVO_LEFT_POS = 1150;
int CURRENT_SERVO_POS = 0;
void moveServo(int pos) {
  if(CURRENT_SERVO_POS != pos) {
    ARM_SERVO.writeMicroseconds(pos);
    delay(2000);
    CURRENT_SERVO_POS = pos;
  }
}


const int ARM_HBRIDGE_IN1_PIN = 8;
const int ARM_HBRIDGE_IN2_PIN = 9;
const int ARM_RETRACTED_SWITCH_PIN = 2;
const int ARM_EXTENDED_SWITCH_PIN = 3;
const long ARM_SHAFT_TIMEOUT = 6000;

boolean moveArm(boolean extend) {
  long curtime = millis();
  if(extend) {
    while((digitalRead(ARM_EXTENDED_SWITCH_PIN) == LOW) && ((millis() - curtime) < ARM_SHAFT_TIMEOUT)) {
      digitalWrite(ARM_HBRIDGE_IN1_PIN, HIGH);
      digitalWrite(ARM_HBRIDGE_IN2_PIN, LOW);
    }
  }
  else {
    while((digitalRead(ARM_RETRACTED_SWITCH_PIN) == LOW) && ((millis() - curtime) < ARM_SHAFT_TIMEOUT)) {
      digitalWrite(ARM_HBRIDGE_IN1_PIN, LOW);
      digitalWrite(ARM_HBRIDGE_IN2_PIN, HIGH);
    }
  }
  digitalWrite(ARM_HBRIDGE_IN1_PIN, LOW);
  digitalWrite(ARM_HBRIDGE_IN2_PIN, LOW);
  
  if(extend && (digitalRead(ARM_EXTENDED_SWITCH_PIN) == LOW)) {
    return false;
  }
  else if(!extend && (digitalRead(ARM_RETRACTED_SWITCH_PIN) == LOW)) {
    return false;
  }
  return true;
}



const int LED_PIN = 13;


void setup() {
  Serial.begin(9600);
  Serial.println("PiController Initialization Started");
  Wire.begin();
  
  pinMode(LED_PIN, OUTPUT);
  pinMode(ARM_HBRIDGE_IN1_PIN, OUTPUT);
  pinMode(ARM_HBRIDGE_IN2_PIN, OUTPUT);
  pinMode(ARM_RETRACTED_SWITCH_PIN, INPUT); 
  pinMode(ARM_EXTENDED_SWITCH_PIN, INPUT); 
  pinMode(ARM_SERVO_PIN, OUTPUT);
  pinMode(DOOR_LEFT_SERVO_PIN, OUTPUT);
  pinMode(DOOR_RIGHT_SERVO_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  digitalWrite(ARM_HBRIDGE_IN1_PIN, LOW);
  digitalWrite(ARM_HBRIDGE_IN2_PIN, LOW);
  digitalWrite(ARM_SERVO_PIN, LOW);
  digitalWrite(DOOR_LEFT_SERVO_PIN, LOW);
  digitalWrite(DOOR_RIGHT_SERVO_PIN, LOW);
  Serial.println("PiController GPIO Initialized");

  ARM_SERVO.writeMicroseconds(ARM_SERVO_CENTER_POS);
  ARM_SERVO.attach(ARM_SERVO_PIN);
  moveServo(ARM_SERVO_CENTER_POS);
  Serial.println("PiController Arm Servo Initialized");
  
  moveArm(false);
  Serial.println("PiController Arm Shaft Initialized");
  
  //moveDoor(false);
  //Serial.println("PiController Arm Door Initialized");

  SCALE_OFFSET = SCALE.read_average(100);
  ENABLE_SCALE = true;
  Serial.println("PiController Scale Initialized");

  delay(1000);  
  digitalWrite(LED_PIN, HIGH);
  Serial.println("PiController Initialization Finished");
}




const int VALVE_CONTROLLER_MAP[30][3] = {
  {ARM_SERVO_CENTER_POS, VALVE_1_CONTROLLER_ADDRESS, 1},
  {ARM_SERVO_CENTER_POS, VALVE_1_CONTROLLER_ADDRESS, 2},
  {ARM_SERVO_CENTER_POS, VALVE_1_CONTROLLER_ADDRESS, 3},
  
  {ARM_SERVO_LEFT_POS, VALVE_1_CONTROLLER_ADDRESS, 4},
  {ARM_SERVO_LEFT_POS, VALVE_1_CONTROLLER_ADDRESS, 5},
  {ARM_SERVO_LEFT_POS, VALVE_1_CONTROLLER_ADDRESS, 6},
  {ARM_SERVO_LEFT_POS, VALVE_1_CONTROLLER_ADDRESS, 7},
  
  {ARM_SERVO_RIGHT_POS, VALVE_1_CONTROLLER_ADDRESS, 8},
  {ARM_SERVO_RIGHT_POS, VALVE_1_CONTROLLER_ADDRESS, 9},
  {ARM_SERVO_RIGHT_POS, VALVE_1_CONTROLLER_ADDRESS, 10},
  {ARM_SERVO_RIGHT_POS, VALVE_1_CONTROLLER_ADDRESS, 11},
  
  {ARM_SERVO_CENTER_POS, VALVE_1_CONTROLLER_ADDRESS, 12},
  {ARM_SERVO_LEFT_POS, VALVE_1_CONTROLLER_ADDRESS, 13},
  {ARM_SERVO_LEFT_POS, VALVE_1_CONTROLLER_ADDRESS, 14},
  {ARM_SERVO_CENTER_POS, VALVE_1_CONTROLLER_ADDRESS, 15},

  {ARM_SERVO_CENTER_POS, VALVE_2_CONTROLLER_ADDRESS, 1},
  {ARM_SERVO_CENTER_POS, VALVE_2_CONTROLLER_ADDRESS, 2},
  {ARM_SERVO_CENTER_POS, VALVE_2_CONTROLLER_ADDRESS, 3},
  
  {ARM_SERVO_LEFT_POS, VALVE_2_CONTROLLER_ADDRESS, 4},
  {ARM_SERVO_LEFT_POS, VALVE_2_CONTROLLER_ADDRESS, 5},
  {ARM_SERVO_LEFT_POS, VALVE_2_CONTROLLER_ADDRESS, 6},
  {ARM_SERVO_LEFT_POS, VALVE_2_CONTROLLER_ADDRESS, 7},
  
  {ARM_SERVO_RIGHT_POS, VALVE_2_CONTROLLER_ADDRESS, 8},
  {ARM_SERVO_RIGHT_POS, VALVE_2_CONTROLLER_ADDRESS, 9},
  {ARM_SERVO_RIGHT_POS, VALVE_2_CONTROLLER_ADDRESS, 10},
  {ARM_SERVO_RIGHT_POS, VALVE_2_CONTROLLER_ADDRESS, 11},
  
  {ARM_SERVO_LEFT_POS, VALVE_2_CONTROLLER_ADDRESS, 12},
  {ARM_SERVO_CENTER_POS, VALVE_2_CONTROLLER_ADDRESS, 13},
  {ARM_SERVO_CENTER_POS, VALVE_2_CONTROLLER_ADDRESS, 14},
  {ARM_SERVO_RIGHT_POS, VALVE_2_CONTROLLER_ADDRESS, 15}
};



#define PACKET_BUF_LEN 256
char PACKET_BUF[PACKET_BUF_LEN];
int PACKET_BUF_PTR = 0;
char INCOMING_PACKET_CHAR;

byte INCOMING_STATUS[16];
void getStatus(int controllerAddress) {
  Wire.requestFrom(controllerAddress, 16);
  delay(100);
  for(int i=0; i<16; i++) {
    if(Wire.available()) {
      INCOMING_STATUS[i] = Wire.read();
    }
  }
}


void loop() {

  /*  
  moveArm(true);
  Serial.print("Extended: ");
  Serial.print(digitalRead(ARM_RETRACTED_SWITCH_PIN));
  Serial.print(" ");
  Serial.print(digitalRead(ARM_EXTENDED_SWITCH_PIN));
  Serial.print(" ");
  Serial.println("");
  delay(500);  
  moveArm(false);
  Serial.print("Retracted: ");
  Serial.print(digitalRead(ARM_RETRACTED_SWITCH_PIN));
  Serial.print(" ");
  Serial.print(digitalRead(ARM_EXTENDED_SWITCH_PIN));
  Serial.print(" ");
  Serial.println("");
  delay(500);
  moveServo(ARM_SERVO_RIGHT_POS);
  delay(500);
  moveServo(ARM_SERVO_LEFT_POS);
  delay(500);
  moveServo(ARM_SERVO_CENTER_POS);
  delay(500);
  return;
  */
  
  while(Serial.available() > 0) {
    INCOMING_PACKET_CHAR = Serial.read();
    if(INCOMING_PACKET_CHAR == '[') {
      PACKET_BUF_PTR = 0;
    }
    PACKET_BUF[PACKET_BUF_PTR] = INCOMING_PACKET_CHAR;
    PACKET_BUF_PTR = min(PACKET_BUF_PTR+1, PACKET_BUF_LEN-2);
    if(INCOMING_PACKET_CHAR == ']' && PACKET_BUF[0] == '[') {
      PACKET_BUF[PACKET_BUF_PTR] = (char)0;
      process_packet();
      PACKET_BUF_PTR = 0;
    }
  }
  if(ENABLE_SCALE) {
    Serial.print("[g");
    Serial.print(getWeight(), DEC);
    Serial.println("]");
  }
}


 

void process_packet() {
    Serial.println(PACKET_BUF);
    if(PACKET_BUF[1] == 'b') {
      moveServo(ARM_SERVO_CENTER_POS);
      moveArm(false);
      moveDoor(false);
      ENABLE_SCALE = false;
      Serial.println("[sAR]");
    }
    if(PACKET_BUF[1] == 'e') {
      Wire.beginTransmission(VALVE_1_CONTROLLER_ADDRESS);
      Wire.write(0);
      Wire.write(0);
      Wire.write(0);
      Wire.endTransmission();
      delay(100);
      Wire.beginTransmission(VALVE_2_CONTROLLER_ADDRESS);
      Wire.write(0);
      Wire.write(0);
      Wire.write(0);
      Wire.endTransmission();
      delay(500);
      moveServo(ARM_SERVO_CENTER_POS);
      moveDoor(true);
      moveArm(true);
      ENABLE_SCALE = true;
      Serial.println("[sAE]");      
    }    
    if(PACKET_BUF[1] == 't') {
      SCALE_OFFSET = SCALE.read_average(100);
      Serial.println("[c]");
    }
    if(PACKET_BUF[1] == 'p') {
      char resp[9] = "[aPP:CC]";
      resp[2] = PACKET_BUF[2];
      resp[3] = PACKET_BUF[3];

      if(!moveArm(false)) {
        resp[5] = 'E';
        resp[6] = 'P';
        Serial.println(resp);
        delay(100);
        return;   
      }
      
      resp[5] = 'B';
      resp[6] = 'M';
      Serial.println(resp);
      delay(100);
      int targetArmServoPos = ARM_SERVO_CENTER_POS;
      switch(PACKET_BUF[3]) {
        case '1':
          targetArmServoPos = ARM_SERVO_CENTER_POS;
          break;
        case '2':
          targetArmServoPos = ARM_SERVO_LEFT_POS;
          break;
        case '3':
          targetArmServoPos = ARM_SERVO_RIGHT_POS;
          break;
        default:
          break;
      }
      moveServo(targetArmServoPos);
      resp[5] = 'E';
      resp[6] = 'M';
      Serial.println(resp);
      delay(100);
      
      resp[5] = 'B';
      resp[6] = 'P';
      Serial.println(resp);
      delay(100);
      unsigned int maxDurationMs = 100;
      for(int i=4; PACKET_BUF[i]=='v'; i = i + 8) {
        byte valveNum = (((unsigned short)PACKET_BUF[i+1])-48)*10+(((unsigned short)PACKET_BUF[i+2])-48)-1;
        unsigned int durationMs = (((unsigned short)PACKET_BUF[i+4])-48)*1000
                                  +(((unsigned short)PACKET_BUF[i+5])-48)*100
                                  +(((unsigned short)PACKET_BUF[i+6])-48)*10
                                  +(((unsigned short)PACKET_BUF[i+7])-48);
        maxDurationMs = max(maxDurationMs, durationMs);
        if(VALVE_CONTROLLER_MAP[valveNum][0] == targetArmServoPos) {
          byte duration_1 = durationMs >> 8;
          byte duration_2 = durationMs & 0xFF;
          Wire.beginTransmission(VALVE_CONTROLLER_MAP[valveNum][1]);
          Wire.write(VALVE_CONTROLLER_MAP[valveNum][2]);
          Wire.write(duration_1);
          Wire.write(duration_2);
          Wire.endTransmission();
          //Serial.print("Turning on valve ");
          //Serial.print(VALVE_CONTROLLER_MAP[valveNum][2], DEC);
          //Serial.print(" for ");
          //Serial.print(durationMs, DEC);
          //Serial.println("milliseconds");
          delay(100);
        }
      }
      //Serial.print("Waiting for ");
      //Serial.print(maxDurationMs+250, DEC);
      //Serial.println("milliseconds");
      delay(maxDurationMs+250);
      
      //Serial.print("Turning off Controller 1");
      Wire.beginTransmission(VALVE_1_CONTROLLER_ADDRESS);
      Wire.write((byte)0x00);
      Wire.write((byte)0xFF);
      Wire.write((byte)0x00);
      Wire.endTransmission();
      delay(100);
      
      //Serial.print("Turning off Controller 2");
      Wire.beginTransmission(VALVE_2_CONTROLLER_ADDRESS);
      Wire.write((byte)0x00);
      Wire.write((byte)0xFF);
      Wire.write((byte)0x00);
      Wire.endTransmission();
      delay(100);
      
      
      resp[5] = 'E';
      resp[6] = 'P';
      Serial.println(resp);
      delay(100);      
    }
}

